= Ultimate AMOS

This is a digital version of the book `Ultimate AMOS`

*Author:* Jason Holborn

*Year:* 1994

*Publisher:* Future Publishing

Released under *Creative Commons* 2019.

== Description

_Different game types and how to program them, how to produce stand-alone programs and where to go for more advice, help and information._

Topics covered include:

* What AMOS can do
* AMOS variants and extensions
* How AMOS works
* Program Planning
* Handling screens
* Hardware and software scrolling
* Handling sprites and "bobs"
* Incorporatiing sound and music
* Using AMAL

== Read this book: 
* https://amigasourcecodepreservation.gitlab.io/ultimate-amos/[Online]
* https://gitlab.com/amigasourcecodepreservation/ultimate-amos/blob/master/pdf/ultimate-amos-1994-holborn.pdf[Scanned pdf]

You can also find the source code examples in the git repository

== How To Generate the Book

The book has been converted to the better-than-markdown https://asciidoctor.org/[AsciiDoc] format, an as such it can possibly be easily converted to many formats. 

In the future, we might we also build the book in more formats (HTML, Epub, Mobi and PDF).

To build:
____

 ./gradlew clean asciidoctor --no-daemon
____

and look under the build/docs/asciidoc/ folder

== Contributing

If you'd like to help out, here is the current to-do:

=== For the ASCIIDOC/ONLINE version

* Proofread the asciidoc and format it (for a good example, see https://gitlab.com/amigasourcecodepreservation/total-amiga-assembler/raw/master/asciidoc/total-amiga-assembler.adoc 
* Format all code examples that are not formated yet.
* Create the index in the asciidoc.
* Create more crossreferences.
* Beautify the book more - minor details, look at other books generated from other AsciiDoc books.

=== For the scanned PDF
* Create a index

=== In general

* Make it autobuild a pdf, epub on checkin to master
* Create an asciidoc html-theme for our project
* Create an asciidoc pdf-theme for our project 



Also, there are some non-direct bugs regarding AsciiDoc and its framework that greatly would help this and future AsciiDoc books if they were resolved.

=== If you have a lot of free time:)

* https://github.com/asciidoctor/asciidoctor-pdf/issues/38[Sane content breaks with PDF generation]
* https://github.com/asciidoctor/asciidoctor/issues/450[Output Index when generating html from AsciiDoctor]
* https://github.com/asciidocfx/AsciidocFX/issues/282[Support for easy styling of output for PDF etc in AsciiDocFx without having hack the docbook.xsl]
* https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=asciidoc[Much better support for AsciiDoc in GitLab]
 

== License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/


== Copyright & Author

Copyright 1994 Jason Holborm
Copyright 1994 Future Publishing (Design and layout)

== Contributors & Thanks

*Many thanks to Jason for releasing the book to the community.*
*Also, thanks to Rod L and Phoebe C for helping me getting permission from Future Publishing.*


